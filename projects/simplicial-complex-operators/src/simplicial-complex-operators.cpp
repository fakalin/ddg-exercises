// Implement member functions for SimplicialComplexOperators class.
#include "simplicial-complex-operators.h"

using namespace geometrycentral;
using namespace geometrycentral::surface;

/*
 * Construct the unsigned vertex-edge adjacency matrix A0.
 *
 * Input:
 * Returns: The sparse vertex-edge adjacency matrix which gets stored in the global variable A0.
 */
SparseMatrix<size_t> SimplicialComplexOperators::buildVertexEdgeAdjacencyMatrix() const {
    SparseMatrix<size_t> mat(mesh->nEdges(), mesh->nVertices());

    for (Edge e : mesh->edges()) {
      for (Vertex v : e.adjacentVertices()) {
        mat.insert(e.getIndex(), v.getIndex()) = 1;
      }
    }

    return mat;
}

/*
 * Construct the unsigned face-edge adjacency matrix A1.
 *
 * Input:
 * Returns: The sparse face-edge adjacency matrix which gets stored in the global variable A1.
 */
SparseMatrix<size_t> SimplicialComplexOperators::buildFaceEdgeAdjacencyMatrix() const {
    SparseMatrix<size_t> mat(mesh->nFaces(), mesh->nEdges());

    for (Face f : mesh->faces()) {
      for (Edge e : f.adjacentEdges()) {
        mat.insert(f.getIndex(), e.getIndex()) = 1;
      }
    }

    return mat;
}

/*
 * Construct a vector encoding the vertices in the selected subset of simplices.
 *
 * Input: Selected subset of simplices.
 * Returns: Vector of length |V|, where |V| = # of vertices in the mesh.
 */
Vector<size_t> SimplicialComplexOperators::buildVertexVector(const MeshSubset& subset) const {
    Vector<size_t> vec = Vector<size_t>::Zero(mesh->nVertices());
    for (size_t i : subset.vertices) {
      vec(i) = 1;
    }
    return vec;
}

/*
 * Construct a vector encoding the edges in the selected subset of simplices.
 *
 * Input: Selected subset of simplices.
 * Returns: Vector of length |E|, where |E| = # of edges in mesh.
 */
Vector<size_t> SimplicialComplexOperators::buildEdgeVector(const MeshSubset& subset) const {
    Vector<size_t> vec = Vector<size_t>::Zero(mesh->nEdges());
    for (size_t i : subset.edges) {
      vec(i) = 1;
    }
    return vec;
}

/*
 * Construct a vector encoding the faces in the selected subset of simplices.
 *
 * Input: Selected subset of simplices.
 * Returns: Vector of length |F|, where |F| = # of faces in mesh.
 */
Vector<size_t> SimplicialComplexOperators::buildFaceVector(const MeshSubset& subset) const {
    Vector<size_t> vec = Vector<size_t>::Zero(mesh->nFaces());
    for (size_t i : subset.faces) {
      vec(i) = 1;
    }
    return vec;
}

namespace {

MeshSubset vectorsToSubset(ManifoldSurfaceMesh* mesh, const Vector<size_t>& vertices, const Vector<size_t>& edges, const Vector<size_t>& faces) {
    MeshSubset subset;

    for (Vertex v : mesh->vertices()) {
      size_t i = v.getIndex();
      if (vertices[i]) {
        subset.addVertex(i);
      }
    }

    for (Edge e : mesh->edges()) {
      size_t i = e.getIndex();
      if (edges[i]) {
        subset.addEdge(i);
      }
    }

    for (Face f : mesh->faces()) {
      size_t i = f.getIndex();
      if (faces[i]) {
        subset.addFace(i);
      }
    }

    return subset;
}

}

/*
 * Compute the simplicial star St(S) of the selected subset of simplices.
 *
 * Input: A MeshSubset object containing the indices of the currently active vertices, edges, and faces, respectively.
 * Returns: The star of the given subset.
 */
MeshSubset SimplicialComplexOperators::star(const MeshSubset& subset) const {
    Vector<size_t> vertices = buildVertexVector(subset);
    Vector<size_t> edges = buildEdgeVector(subset) + A0 * vertices;
    Vector<size_t> faces = buildFaceVector(subset) + A1 * edges;
    return vectorsToSubset(mesh, vertices, edges, faces);
}


/*
 * Compute the closure Cl(S) of the selected subset of simplices.
 *
 * Input: A MeshSubset object containing the indices of the currently active vertices, edges, and faces, respectively.
 * Returns: The closure of the given subset.
 */
MeshSubset SimplicialComplexOperators::closure(const MeshSubset& subset) const {
    Vector<size_t> faces = buildFaceVector(subset);
    Vector<size_t> edges = buildEdgeVector(subset) + A1.transpose() * faces;
    Vector<size_t> vertices = buildVertexVector(subset) + A0.transpose() * edges;
    return vectorsToSubset(mesh, vertices, edges, faces);
}

/*
 * Compute the link Lk(S) of the selected subset of simplices.
 *
 * Input: A MeshSubset object containing the indices of the currently active vertices, edges, and faces, respectively.
 * Returns: The link of the given subset.
 */
MeshSubset SimplicialComplexOperators::link(const MeshSubset& subset) const {

    MeshSubset link = closure(star(subset));
    link.deleteSubset(star(closure(subset)));
    return link;
}

/*
 * Return true if the selected subset is a simplicial complex, false otherwise.
 *
 * Input: A MeshSubset object containing the indices of the currently active vertices, edges, and faces, respectively.
 * Returns: True if given subset is a simplicial complex, false otherwise.
 */
bool SimplicialComplexOperators::isComplex(const MeshSubset& subset) const {
    return subset.equals(closure(subset));
}

/*
 * Check if the given subset S is a pure simplicial complex. If so, return the degree of the complex. Otherwise, return
 * -1.
 *
 * Input: A MeshSubset object containing the indices of the currently active vertices, edges, and faces, respectively.
 * Returns: int representing the degree of the given complex (-1 if not pure)
 */
int SimplicialComplexOperators::isPureComplex(const MeshSubset& subset) const {
  if (!isComplex(subset)) {
    return -1;
  }

  // No vertices means no edges nor faces, since subset is a complex.
  if (subset.vertices.empty()) {
    return -1;
  }

  // No edges means no faces, since subset is a complex. Thus, subset is
  // a collection of vertices.
  if (subset.edges.empty()) {
    return 0;
  }

  for (size_t vertex_index : subset.vertices) {
    bool found_edge = false;
    for (SparseMatrix<size_t>::InnerIterator it(A0, vertex_index); it; ++it) {
      if (subset.edges.find(it.row()) != subset.edges.end()) {
        found_edge = true;
        break;
      }
    }
    if (!found_edge) {
      return -1;
    }
  }

  // Subset has no faces, so it must only be a collection of vertices
  // contained in some edge.
  if (subset.faces.empty()) {
    return 1;
  }

  for (size_t edge_index : subset.edges) {
    bool found_face = false;
    for (SparseMatrix<size_t>::InnerIterator it(A1, edge_index); it; ++it) {
      if (subset.faces.find(it.row()) != subset.faces.end()) {
        found_face = true;
        break;
      }
    }
    if (!found_face) {
      return -1;
    }
  }

  return 2;
}

/*
 * Compute the set of simplices contained in the boundary bd(S) of the selected subset S of simplices.
 *
 * Input: A MeshSubset object containing the indices of the currently active vertices, edges, and faces, respectively.
 * Returns: The boundary of the given subset.
p */
MeshSubset SimplicialComplexOperators::boundary(const MeshSubset& subset) const {
    int degree = isPureComplex(subset);
    if (degree == -1) {
      return MeshSubset();
    }

    // A collection of vertices has no boundary.
    if (degree == 0) {
      return MeshSubset();
    }

    MeshSubset boundary;

    if (degree == 1) {
      for (size_t vertex_index : subset.vertices) {
        size_t num_edges = 0;
        for (SparseMatrix<size_t>::InnerIterator it(A0, vertex_index); it; ++it) {
          if (subset.edges.find(it.row()) != subset.edges.end()) {
            ++num_edges;
          }
          if (num_edges >= 2) {
            break;
          }
        }
        if (num_edges == 1) {
          boundary.addVertex(vertex_index);
        }
      }
    } else {
      for (size_t edge_index : subset.edges) {
        size_t num_faces = 0;
        for (SparseMatrix<size_t>::InnerIterator it(A1, edge_index); it; ++it) {
          if (subset.faces.find(it.row()) != subset.faces.end()) {
            ++num_faces;
          }
          if (num_faces >= 2) {
            break;
          }
        }
        if (num_faces == 1) {
          boundary.addEdge(edge_index);
        }
      }
    }

    return closure(boundary);
}
